print('Start #################################################################');

db = db.getSiblingDB('SampleDB');
db.createUser(
  {
    user: 'root',
    pwd: 'root1234',
    roles: [{ role: 'readWrite', db: 'api_prod_db' }],
  },
);
db.createCollection('sampleCollection');

print('END #################################################################');