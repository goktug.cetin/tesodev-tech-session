package main

import (
	"fmt"
	"os"

	echo "github.com/labstack/echo/v4"
)

func main() {

	runEnv := os.Getenv("RUN_ENVIRONMENT")
	if runEnv == "docker" {
		fmt.Print("Running in docker")
	} else {
		fmt.Print("Running locally")
	}

	e := echo.New()
	e.GET("/", func(c echo.Context) error {
		return c.String(200, "Welcome to Main Route of the API on port 8000")
	})
	e.Logger.Fatal(e.Start(":8000"))

}
